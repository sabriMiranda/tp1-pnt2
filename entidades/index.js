const Sequelize = require('sequelize');
const Sucursal = require('./sucursal');
const Empleado = require('./empleado');
const Vehiculo = require('./vehiculo');
const mainDb = {};

const sequelize = new Sequelize('mainDb', null, null, {
    dialect: "sqlite",
    storage: "./mainDB.sqlite"
});


sequelize.authenticate()
    .then(function() { console.log('Autenticado'); })
    .catch(function() {console.log('Error autenticado'); })

    mainDb.Empleado = Empleado(sequelize, Sequelize);
    mainDb.Sucursal = Sucursal(sequelize,Sequelize);
    mainDb.Vehiculo = Vehiculo(sequelize,Sequelize);
    
    
    

sequelize.sync({ force: true})
    .then(function(err){
            console.log('Modelo sincronizado');
    })
    .catch(function(err){
            console.log('Error al sincronizar');
    })